/**
 * -------------------------------------------------------------------------- *
 * xbockp00_xbogda02.sql - PL/SQL Database Script
 * -------------------------------------------------------------------------- *
 *
 * Course: Database Systems (IDS)
 * Authors: Pavol Bock <xbockp00@stud.fit.vutbr.cz>
 *          Iaroslav Bogdanchikov <xbogda02@stud.fit.vutbr.cz>
 *
 * -------------------------------------------------------------------------- *
 *
 * This file implements a PL/SQL script for Oracle 12c Database Management
 * System (DBMS). The script allows repeated calls, i.e. deleting and
 * re-creating or overwriting objects in the database and their data.
 *
 * -------------------------------------------------------------------------- *
 */

DROP MATERIALIZED VIEW DishesIngredientsInfoMV;
DROP MATERIALIZED VIEW LOG ON DishesIngredients;
DROP MATERIALIZED VIEW LOG ON Ingredients;

-- Delete all database tables. This allows repeated calls of the script.
BEGIN
  FOR MyTable IN (SELECT TABLE_NAME FROM USER_TABLES)
  LOOP
    EXECUTE IMMEDIATE 'DROP TABLE ' || MyTable.TABLE_NAME || ' CASCADE CONSTRAINTS';
  END LOOP;

  FOR MySequence IN (SELECT SEQUENCE_NAME FROM USER_SEQUENCES)
  LOOP
    EXECUTE IMMEDIATE 'DROP SEQUENCE ' || MySequence.SEQUENCE_NAME;
  END LOOP;

  FOR MyIndex IN (SELECT INDEX_NAME
                  FROM USER_INDEXES
                  WHERE INDEX_NAME IN ('DISHPRICEI'))
  LOOP
    EXECUTE IMMEDIATE 'DROP INDEX ' || MyIndex.INDEX_NAME;
  END LOOP;
END;
/

-- Create database tables.
CREATE TABLE Customers (
  Email VARCHAR(254) NOT NULL,
  FirstName VARCHAR(30) NOT NULL,
  LastName VARCHAR(30) NOT NULL,
  PhoneNumber NUMBER(15) NOT NULL,

  CONSTRAINT CustomerID PRIMARY KEY (Email),
  CONSTRAINT CustomersEmailCK CHECK (REGEXP_LIKE(Email, '^.{1,64}@.{1,189}$'))
);

CREATE TABLE Employees (
  Email VARCHAR(254) NOT NULL,
  FirstName VARCHAR(30) NOT NULL,
  LastName VARCHAR(30) NOT NULL,
  Login VARCHAR(15) NOT NULL UNIQUE,
  Password VARCHAR(128) NOT NULL,
  BankAccountNumber VARCHAR(29) NOT NULL UNIQUE,
  EmployeeContract NUMBER NOT NULL UNIQUE,
  JobTitle VARCHAR(30) NOT NULL,

  CONSTRAINT EmployeeID PRIMARY KEY (Email),
  CONSTRAINT EmployeesEmailCK CHECK (REGEXP_LIKE(Email, '^.{1,64}@.{1,189}$')),
  CONSTRAINT EmployeesPasswordCK CHECK (REGEXP_LIKE(Password, '^.*[0-9]', 'c') -- At least one number.
                                        AND REGEXP_LIKE(Password, '^.*[A-Z]', 'c') -- At least one uppercase letter.
                                        AND REGEXP_LIKE(Password, '^.*[#$^+=!*()@%&]', 'c') -- At least one special character.
                                        AND REGEXP_LIKE(Password, '[0-9a-zA-Z#$^+=!*()@%&]{8,}$', 'c')), -- Minimum length is 8.
  CONSTRAINT EmployeesBankAccountNumberCK CHECK (REGEXP_LIKE(BankAccountNumber, '^CZ\d{2}\s?([0-9]{4}\s?){5}$'))
);

CREATE TABLE DiningAreas (
  AreaName VARCHAR(20) NOT NULL,
  NumberOfTables NUMBER NOT NULL,
  IsReserved NUMBER(1) DEFAULT 0 NOT NULL,

  CONSTRAINT DiningAreaID PRIMARY KEY (AreaName),
  CONSTRAINT DiningAreasIsReservedCK CHECK (REGEXP_LIKE(IsReserved, '^[0-1]$'))
);

CREATE TABLE Tables (
  TableNumber NUMBER NOT NULL,
  Capacity NUMBER NOT NULL,
  IsReserved NUMBER(1) DEFAULT 0 NOT NULL,
  DiningArea VARCHAR(20) NOT NULL,

  CONSTRAINT TableID PRIMARY KEY (TableNumber),
  CONSTRAINT TablesDiningAreaFK FOREIGN KEY (DiningArea) REFERENCES DiningAreas(AreaName),
  CONSTRAINT TablesIsReservedCK CHECK (REGEXP_LIKE(IsReserved, '^[0-1]$'))
);

CREATE TABLE Reservations (
  ReservationID NUMBER NOT NULL,
  DateAndTime TIMESTAMP NOT NULL,
  Customer VARCHAR(254) NOT NULL,
  TableNumber NUMBER,
  DiningArea VARCHAR(20),

  CONSTRAINT ReservationID PRIMARY KEY (ReservationID),
  CONSTRAINT CustomersTablesReservationsCustomerFK FOREIGN KEY (Customer) REFERENCES Customers(Email),
  CONSTRAINT CustomersTablesReservationsTableNumberFK FOREIGN KEY (TableNumber) REFERENCES Tables(TableNumber),
  CONSTRAINT CustomersTablesReservationsDiningAreaFK FOREIGN KEY (DiningArea) REFERENCES DiningAreas(AreaName)
);

CREATE TABLE Bills (
  BillNumber NUMBER NOT NULL,
  DateAndTime TIMESTAMP NOT NULL,
  IsReserved NUMBER(1) DEFAULT 0 NOT NULL,
  Employee VARCHAR(254) NOT NULL,

  CONSTRAINT BillID PRIMARY KEY (BillNumber),
  CONSTRAINT BillsEmployeeFK FOREIGN KEY (Employee) REFERENCES Employees(Email),
  CONSTRAINT BillsIsReservedCK CHECK (REGEXP_LIKE(IsReserved, '^[0-1]$'))
);

CREATE TABLE Dishes (
  Name VARCHAR(50) NOT NULL,
  Quantity NUMBER NOT NULL,
  Price NUMBER NOT NULL,
  Employee VARCHAR(254) NOT NULL,

  CONSTRAINT DishID PRIMARY KEY (Name),
  CONSTRAINT DishesEmployeeFK FOREIGN KEY (Employee) REFERENCES Employees(Email)
);

CREATE TABLE Orders (
  OrderNumber NUMBER NOT NULL,
  DateAndTime TIMESTAMP NOT NULL,
  Employee VARCHAR(254) NOT NULL,
  BillNumber NUMBER NOT NULL,

  CONSTRAINT OrderID PRIMARY KEY (OrderNumber),
  CONSTRAINT OrdersEmployeeFK FOREIGN KEY (Employee) REFERENCES Employees(Email),
  CONSTRAINT OrdersBillFK FOREIGN KEY (BillNumber) REFERENCES Bills(BillNumber)
);

CREATE TABLE TablesOrders (
  TableNumber NUMBER NOT NULL,
  OrderNumber NUMBER NOT NULL,

  CONSTRAINT TablesOrdersID PRIMARY KEY (TableNumber, OrderNumber),
  CONSTRAINT TablesOrdersTableNumberFK FOREIGN KEY (TableNumber) REFERENCES Tables(TableNumber),
  CONSTRAINT TablesOrdersOrderNumberFK FOREIGN KEY (OrderNumber) REFERENCES Orders(OrderNumber)
);

CREATE TABLE OrdersDishes (
  OrderNumber NUMBER NOT NULL,
  Dish VARCHAR(50) NOT NULL,

  CONSTRAINT OrdersDishesID PRIMARY KEY (OrderNumber, Dish),
  CONSTRAINT OrdersDishesOrderNumberFK FOREIGN KEY (OrderNumber) REFERENCES Orders(OrderNumber),
  CONSTRAINT OrdersDishesDishFK FOREIGN KEY (Dish) REFERENCES Dishes(Name)
);

CREATE TABLE Ingredients (
  Name VARCHAR(50) NOT NULL,
  Kind VARCHAR(30),
  Allergens NUMBER(2) DEFAULT 0 NOT NULL,
  DPH NUMBER NOT NULL,

  CONSTRAINT IngredientID PRIMARY KEY (Name),
  CONSTRAINT IngredientsAllergensCK CHECK (REGEXP_LIKE(Allergens, '^(([0-9])|(1[0-4]))$'))
);

CREATE TABLE DishesIngredients (
  Dish VARCHAR(50) NOT NULL,
  Ingredient VARCHAR(50) NOT NULL,

  CONSTRAINT DishesIngredientsID PRIMARY KEY (Dish, Ingredient),
  CONSTRAINT DishesIngredientsDishFK FOREIGN KEY (Dish) REFERENCES Dishes(Name),
  CONSTRAINT DishesIngredientsIngredientFK FOREIGN KEY (Ingredient) REFERENCES Ingredients(Name)
);

-- Trigger that picks first available table, if it wasn't chosen explicitly.
CREATE OR REPLACE TRIGGER PickFreeTable BEFORE INSERT ON Reservations
  FOR EACH ROW
  WHEN (NEW.TableNumber IS NULL AND NEW.DiningArea IS NULL)
  BEGIN
    SELECT MIN(T.TableNumber)
    INTO :NEW.TableNumber
    FROM Tables T
    WHERE T.TableNumber NOT IN (
      SELECT R.TableNumber
      FROM Reservations R
      WHERE R.DateAndTime = :NEW.DateAndTime AND R.TABLENUMBER IS NOT NULL
    ) AND T.TableNumber NOT IN (
          SELECT DISTINCT T.TABLENUMBER
          FROM Reservations R JOIN DININGAREAS D on R.DININGAREA = D.AREANAME JOIN TABLES T on D.AREANAME = T.DININGAREA
          WHERE R.DateAndTime = :NEW.DateAndTime
      );

    IF (:NEW.TableNumber IS NULL) THEN
        RAISE_APPLICATION_ERROR(-20001, 'No free tables available.');
    END IF;
  END;
/

-- Sequence for generating order numbers.
CREATE SEQUENCE OrderNumberSeq
    START WITH 1
    INCREMENT BY 1;

-- Trigger that assigns number to a new order using 'OrderNumberSeq' sequence.
CREATE OR REPLACE TRIGGER GenerateOrderNumber BEFORE INSERT ON Orders
    FOR EACH ROW
    BEGIN
        IF (:NEW.OrderNumber IS NULL) THEN
          :NEW.OrderNumber := OrderNumberSeq.NEXTVAL;
        END IF;
    END;
/

-- Procedure that applies a chosen discount rate to the dishes that contain a chosen ingredient.
CREATE OR REPLACE PROCEDURE ApplyDiscount(DiscountRate IN NUMBER, IngredientName IN Ingredients.Name%TYPE)
AS
  CURSOR DishesPricesCur IS
  SELECT Dish, Price
    FROM DishesIngredients DI JOIN Ingredients I ON DI.Ingredient = I.Name JOIN Dishes D ON DI.Dish = D.Name
    WHERE Ingredient = ApplyDiscount.IngredientName;

    DishPrice DishesPricesCur%ROWTYPE;
    NewPrice Dishes.Price%TYPE;
BEGIN
  OPEN DishesPricesCur;

  LOOP
    FETCH DishesPricesCur INTO DishPrice;
    EXIT WHEN DishesPricesCur%NOTFOUND;

    NewPrice := (DishPrice.Price - DishPrice.Price * DiscountRate * 0.01);

    UPDATE Dishes SET Price = NewPrice WHERE Dishes.Name = DishPrice.Dish;
  END LOOP;

  CLOSE DishesPricesCur;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
      DBMS_OUTPUT.PUT_LINE('No data found.');
    WHEN OTHERS THEN
      DBMS_OUTPUT.PUT_LINE('Unknown error occured.');
END;
/

-- Procedure that provides information about reservations based on first name and last name of a customer.
CREATE OR REPLACE PROCEDURE GetReservationsInfo(FirstName IN Customers.FirstName%TYPE, LastName IN Customers.LastName%TYPE)
AS
  CURSOR ReservationsInfoCur IS
    SELECT DateAndTime, TableNumber, DiningArea
    FROM Customers C JOIN Reservations R ON C.Email = R.Customer
    WHERE FirstName = GetReservationsInfo.FirstName AND LastName = GetReservationsInfo.LastName
    ORDER BY DateAndTime;

  ReservationInfo ReservationsInfoCur%ROWTYPE;
BEGIN
  DBMS_OUTPUT.PUT_LINE('Reservations for ' || GetReservationsInfo.FirstName || ' ' || GetReservationsInfo.LastName || ':');

  OPEN ReservationsInfoCur;

  LOOP
    FETCH ReservationsInfoCur INTO ReservationInfo;

    IF (ReservationsInfoCur%ROWCOUNT = 0) THEN
      DBMS_OUTPUT.PUT_LINE('No reservations found.');
    END IF;

    EXIT WHEN ReservationsInfoCur%NOTFOUND;

    DBMS_OUTPUT.PUT('Date: ' || TO_CHAR(ReservationInfo.DateAndTime, 'DD-MON-YYYY HH24:MI'));

    IF (ReservationInfo.TableNumber IS NOT NULL) THEN
      DBMS_OUTPUT.PUT_LINE(' Table Number: ' || ReservationInfo.TableNumber);
    ELSIF (ReservationInfo.DiningArea IS NOT NULL) THEN
      DBMS_OUTPUT.PUT_LINE(' Dining Area: ' || ReservationInfo.DiningArea);
    END IF;
  END LOOP;

  CLOSE ReservationsInfoCur;
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Unknown error occured.');
END;
/

-- Insert sample data into the tables.
INSERT ALL
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('dmcwilliams1@reference.com', 'Derry', 'McWilliams', '2613429889')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('jbradberry0@dropbox.com', 'Joanna', 'Bradberry', '6906400851')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('tkimbury2@va.gov', 'Tobias', 'Kimbury', '3111180828')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('kmosedill3@so-net.ne.jp', 'Keriann', 'Mosedill', '9629263797')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('dcampling4@businessinsider.com', 'Davie', 'Campling', '3519237681')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('ldoll5@de.vu', 'Lenka', 'Doll', '6352303932')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('kleavesley6@woothemes.com', 'Kelwin', 'Leavesley', '2567077426')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('cpuleston7@ox.ac.uk', 'Cam', 'Puleston', '4796651035')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('akahen8@home.pl', 'Ally', 'Kahen', '5448691889')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('jbleibaum9@addthis.com', 'Jeannette', 'Bleibaum', '7472232175')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('ktukesbya@dedecms.com', 'Karon', 'Tukesby', '5042565422')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('kpyburnb@nba.com', 'Kizzee', 'Pyburn', '5649013340')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('cproudc@senate.gov', 'Cornela', 'Proud', '1104327185')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('crampleyd@yellowbook.com', 'Caroline', 'Rampley', '3581108098')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('ccrookshanke@sakura.ne.jp', 'Chevy', 'Crookshank', '5592939258')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('jtomaskunasf@state.gov', 'Julia', 'Tomaskunas', '8172003762')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('relstoneg@purevolume.com', 'Ricky', 'Elstone', '4875845594')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('nsearchh@xinhuanet.com', 'Norrie', 'Search', '7421621610')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('enappini@thetimes.co.uk', 'Elvera', 'Nappin', '6277921950')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('cmcshiriej@1und1.de', 'Camilla', 'McShirie', '6988059904')
  INTO Customers (Email, FirstName, LastName, PhoneNumber)
  VALUES ('pavey@restaurant.com', 'Stephanus', 'Pavey', '420773123123')
SELECT * FROM DUAL;

INSERT ALL
  INTO Employees (Email, FirstName, LastName, Login, Password, BankAccountNumber, EmployeeContract, JobTitle)
  VALUES ('pavey@restaurant.com', 'Stephanus', 'Pavey', 'pavey', 'lbJajiw$1tDbGf!X82a6', 'CZ6508000000192000145300', 1, 'Owner')
  INTO Employees (Email, FirstName, LastName, Login, Password, BankAccountNumber, EmployeeContract, JobTitle)
  VALUES ('rycroft@restaurant.com', 'Erv', 'Rycroft', 'rycroft', 'm?HbrZ06|Bv=t4Ltv+wy', 'CZ6508000000192000145301', 2, 'Manager')
  INTO Employees (Email, FirstName, LastName, Login, Password, BankAccountNumber, EmployeeContract, JobTitle)
  VALUES ('wrightem@restaurant.com', 'Benoit', 'Wrightem', 'wrightem', 'jhQB0ac^GHi6ahf$Ee+K', 'CZ6508000000192000145302', 3, 'Manager')
  INTO Employees (Email, FirstName, LastName, Login, Password, BankAccountNumber, EmployeeContract, JobTitle)
  VALUES ('clarkin@restaurant.com', 'Maxy', 'Clarkin', 'clarkin', '+0vUVbL==?1QqMbXSJ#$', 'CZ6508000000192000145303', 4, 'Waiter')
  INTO Employees (Email, FirstName, LastName, Login, Password, BankAccountNumber, EmployeeContract, JobTitle)
  VALUES ('haffner@restaurant.com', 'Urbain', 'Haffner', 'haffner', 'eMDKT2#|oRkk%8y6*m!V', 'CZ6508000000192000145304', 5, 'Waiter')
  INTO Employees (Email, FirstName, LastName, Login, Password, BankAccountNumber, EmployeeContract, JobTitle)
  VALUES ('dando@restaurant.com', 'Annabel', 'Dando', 'dando', 'zE5p31F$E&XdkoAixQMS', 'CZ6508000000192000145305', 6, 'Waiter')
  INTO Employees (Email, FirstName, LastName, Login, Password, BankAccountNumber, EmployeeContract, JobTitle)
  VALUES ('robardey@restaurant.com', 'Verna', 'Robardey', 'robardey', 'J6Q!XiwU7wWCqPb&jysx', 'CZ6508000000192000145306', 7, 'Waiter')
  INTO Employees (Email, FirstName, LastName, Login, Password, BankAccountNumber, EmployeeContract, JobTitle)
  VALUES ('macphail@restaurant.com', 'Rori', 'MacPhail', 'macphail', 'n&9!oFFnwQ^ULK=4X=vB', 'CZ6508000000192000145307', 8, 'Waiter')
  INTO Employees (Email, FirstName, LastName, Login, Password, BankAccountNumber, EmployeeContract, JobTitle)
  VALUES ('maydwell@restaurant.com', 'Tymon', 'Maydwell', 'maydwell', 'dpHCU?jx=Ct1@!L!$Ob9', 'CZ6508000000192000145308', 9, 'Waiter')
  INTO Employees (Email, FirstName, LastName, Login, Password, BankAccountNumber, EmployeeContract, JobTitle)
  VALUES ('martinon@restaurant.com', 'Clywd', 'Martinon', 'martinon', '%Yd++jTh^mBYNfRz52ol', 'CZ6508000000192000145309', 10, 'Waiter')
SELECT * FROM DUAL;

INSERT ALL
  INTO DiningAreas (AreaName, NumberOfTables, IsReserved) VALUES ('Alfredo', 3, 0)
  INTO DiningAreas (AreaName, NumberOfTables, IsReserved) VALUES ('Bruno', 2, 0)
  INTO DiningAreas (AreaName, NumberOfTables, IsReserved) VALUES ('Carlo', 7, 0)
  INTO DiningAreas (AreaName, NumberOfTables, IsReserved) VALUES ('Edoardo', 4, 0)
SELECT * FROM DUAL;

INSERT ALL
  INTO Tables (TableNumber, Capacity, IsReserved, DiningArea) VALUES (1, 8, 0, 'Alfredo')
  INTO Tables (TableNumber, Capacity, IsReserved, DiningArea) VALUES (2, 8, 0, 'Alfredo')
  INTO Tables (TableNumber, Capacity, IsReserved, DiningArea) VALUES (3, 8, 0, 'Alfredo')
  INTO Tables (TableNumber, Capacity, IsReserved, DiningArea) VALUES (4, 12, 0, 'Bruno')
  INTO Tables (TableNumber, Capacity, IsReserved, DiningArea) VALUES (5, 12, 0, 'Bruno')
  INTO Tables (TableNumber, Capacity, IsReserved, DiningArea) VALUES (6, 4, 0, 'Carlo')
  INTO Tables (TableNumber, Capacity, IsReserved, DiningArea) VALUES (7, 4, 0, 'Carlo')
  INTO Tables (TableNumber, Capacity, IsReserved, DiningArea) VALUES (8, 4, 0, 'Carlo')
  INTO Tables (TableNumber, Capacity, IsReserved, DiningArea) VALUES (9, 4, 0, 'Carlo')
  INTO Tables (TableNumber, Capacity, IsReserved, DiningArea) VALUES (10 ,4, 0, 'Carlo')
  INTO Tables (TableNumber, Capacity, IsReserved, DiningArea) VALUES (11, 4, 0, 'Carlo')
  INTO Tables (TableNumber, Capacity, IsReserved, DiningArea) VALUES (12, 4, 0, 'Carlo')
  INTO Tables (TableNumber, Capacity, IsReserved, DiningArea) VALUES (13, 6, 0, 'Edoardo')
  INTO Tables (TableNumber, Capacity, IsReserved, DiningArea) VALUES (14, 6, 0, 'Edoardo')
  INTO Tables (TableNumber, Capacity, IsReserved, DiningArea) VALUES (15, 6, 0, 'Edoardo')
  INTO Tables (TableNumber, Capacity, IsReserved, DiningArea) VALUES (16, 6, 0, 'Edoardo')
SELECT * FROM DUAL;

INSERT ALL
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (1, '20-12-2017 19:03:09', 'dmcwilliams1@reference.com', 1, NULL)
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (2, '15-06-2017 16:34:28', 'jbradberry0@dropbox.com', 9, NULL)
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (3, '19-08-2017 20:38:24', 'tkimbury2@va.gov', 13, NULL)
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (4, '22-10-2017 01:20:03', 'kmosedill3@so-net.ne.jp', 11, NULL)
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (5, '28-11-2017 21:11:27', 'dcampling4@businessinsider.com', 16, NULL)
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (6, '28-11-2017 21:11:27', 'ldoll5@de.vu', 4, NULL)
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (7, '30-08-2017 12:28:57', 'kleavesley6@woothemes.com', NULL, 'Bruno')
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (8, '15-06-2017 16:34:28', 'cpuleston7@ox.ac.uk', NULL, 'Carlo')
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (9, '28-11-2017 21:11:27', 'akahen8@home.pl', NULL, 'Edoardo')
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (10, '03-07-2017 14:16:48', 'jbleibaum9@addthis.com', 11, NULL)
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (11, '26-04-2017 21:10:23', 'ktukesbya@dedecms.com', NULL, 'Edoardo')
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (12, '21-07-2017 17:46:36', 'kpyburnb@nba.com', NULL, 'Bruno')
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (13, '20-12-2017 19:03:09', 'cproudc@senate.gov', NULL, 'Carlo')
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (14, '20-12-2017 19:03:09', 'crampleyd@yellowbook.com', NULL, 'Alfredo')
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (15, '21-07-2017 17:46:36', 'ccrookshanke@sakura.ne.jp', NULL, 'Edoardo')
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (16, '08-09-2017 02:33:54', 'jtomaskunasf@state.gov', NULL, 'Alfredo')
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (17, '20-12-2017 19:03:09', 'relstoneg@purevolume.com', NULL, 'Carlo')
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (18, '26-04-2017 21:10:23', 'nsearchh@xinhuanet.com', NULL, 'Bruno')
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (19, '30-07-2017 01:23:59', 'enappini@thetimes.co.uk', 15, NULL)
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (20, '21-07-2017 17:46:36', 'cmcshiriej@1und1.de', 7, NULL)
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (21, '28-11-2017 21:11:27', 'dmcwilliams1@reference.com', NULL, 'Edoardo')
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (22, '08-09-2017 02:33:54', 'jbradberry0@dropbox.com', NULL, 'Carlo')
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (23, '08-02-2018 17:47:31', 'tkimbury2@va.gov', 10, NULL)
  INTO Reservations (ReservationID, DateAndTime, Customer, TableNumber, DiningArea)
  VALUES (24, '31-03-2018 19:00:00', 'pavey@restaurant.com', 13, NULL)
SELECT * FROM DUAL;

-- Disclaimer:
-- Inserted dishes are hugely inspired by the dishes in the menu of "Jamie's Italian Restaurant".
-- They're inserted exclusively for demonstration as sample data and are not going to be used
-- in the real world in any way. Any possible copyrighted names of the dishes that could appear below
-- belong to JAMIE OLIVER ENTERPRISES LIMITED.
INSERT ALL
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Bread Board', 9, 4, 'wrightem@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Big Green Olives on Ice', 42, 4, 'wrightem@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Cheese Croquettes', 24, 5, 'wrightem@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Crispy Polenta Chips', 41, 4, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Italian Nachos', 16, 4.5, 'wrightem@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Creamy Burrata', 15, 7.2, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Buttermilk Squid', 44, 7.3, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Margherita Arancini', 22, 6, 'rycroft@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Meatballs', 7, 7, 'wrightem@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Vegetable Antipasti Plate', 22, 6.9, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Meat Plank', 20, 15, 'rycroft@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Tagliatelle Bolognese', 22, 6.8, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Lasagne', 38, 13.5, 'wrightem@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Prawn Linguine', 30, 8.3, 'wrightem@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Carbonara', 13, 6.8, 'wrightem@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Spaghetti & Meatballs', 4, 7, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Gnocchi Pomodoro', 10, 6.8, 'rycroft@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Spinach & Ricotta Ravioli', 31, 12.5, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Mushroom Ravioli', 7, 13, 'wrightem@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Octopus Ravioli', 25, 15, 'wrightem@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Grilled Chicken Under A Brick', 25, 14.5, 'rycroft@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Truffled Chicken Under A Brick', 13, 16.5, 'rycroft@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Italian Burger', 35, 14.9, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Veggie Quinoa & Kale Burger', 44, 14, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('British Trout', 4, 15.5, 'rycroft@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Flash Steak', 12, 17.5, 'rycroft@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Sirloin Steak', 20, 23, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Salad', 33, 6, 'wrightem@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Kale Salad', 33, 6, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Pizza Julietta', 19, 11.5, 'rycroft@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Pizza Pepperoni', 35, 13, 'rycroft@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Pizza Four Cheese', 28, 12.5, 'wrightem@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Sweet Potato Fries', 41, 4, 'rycroft@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Kale & Hazelnut Salad', 30, 4, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Roasted Broccoli', 39, 4, 'wrightem@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Skinny Fries', 6, 4, 'wrightem@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Molten Chocolate Praline Pudding', 23, 6.5, 'wrightem@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Chocolate Brownie', 15, 6, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Baked Lemon & Ricotta Cheesecake', 28, 5.9, 'rycroft@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Torta Di Nada', 47, 5.9, 'rycroft@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Tiramisù', 28, 6.5, 'rycroft@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Affogato', 48, 5, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Gelato & Sorbet', 12, 5, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Fruit Juice', 16, 3, 'wrightem@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Homemade Lemonade', 15, 3.2, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Espresso', 48, 2.3, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Cappuccino', 31, 2.9, 'pavey@restaurant.com')
  INTO Dishes (Name, Quantity, Price, Employee) VALUES ('Tea', 27, 2.4, 'wrightem@restaurant.com')
SELECT * FROM DUAL;

INSERT ALL
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Apple', 'Fruit', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Apple Juice', 'Juice', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Apricot', 'Fruit', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Aubergine', 'Vegetable', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Avocado', 'Vegetable', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Basil', 'Herb', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Beef', 'Meat', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Beef Steak', 'Meat', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Beetroot', 'Herb', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Black Olive', 'Vegetable', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Blueberry', 'Fruit', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Broccoli', 'Vegetable', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Brown Sugar', 'Condiment', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Burrata', 'Dairy', 7, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Butter', 'Dairy', 7, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Buttermilk', 'Dairy', 7, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Calamari', 'Seafood', 8, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Caper', 'Herb', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Cappuccino', 'Coffee', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Carrot', 'Vegetable', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Casarecce', 'Cereal', 2, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Celery', 'Celery', 1, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Cheddar', 'Dairy', 7, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Chicken', 'Poultry', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Chilli', 'Vegetable', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Chocolate', 'Chocolate', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Cocoa', 'Chocolate', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Coffee', 'Coffee', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Cornmeal', 'Cereal', 2, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Cottage Cheese', 'Dairy', 7, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Courgette', 'Vegetable', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Crab', 'Seafood', 3, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Cranberry', 'Fruit', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Cranberry Juice', 'Juice', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Cream', 'Dairy', 7, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Cream Cheese', 'Dairy', 7, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Cumin', 'Herb', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Egg', 'Egg', 3, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Espresso', 'Coffee', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Fennel', 'Herb', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Fontina', 'Dairy', 7, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Garlic', 'Herb', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Gelato', 'Dairy', 7, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Grape', 'Fruit', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Grapefruit', 'Fruit', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Green Olive', 'Vegetable', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Harissa', 'Vegetable', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Hazelnut', 'Nut', 10, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Honey', 'Condiment', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Kale', 'Vegetable', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Lasagne', 'Cereal', 2, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Lemon', 'Fruit', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Lemonade', 'Beverage', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Lentils', 'Cereal', 2, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Linguine', 'Cereal', 2, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Mascarpone', 'Dairy', 7, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Milk', 'Dairy', 7, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Mint', 'Herb', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Mollusc', 'Seafood', 8, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Mortadella', 'Meat', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Mozzarella', 'Dairy', 7, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Mushroom', 'Fungus', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Mustard', 'Condiment', 9, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Nuts', 'Nut', 10, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Octopus', 'Seafood', 8, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Oil', 'Oil', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Olive', 'Vegetable', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Olive Oil', 'Oil', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Onion', 'Vegetable', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Orange', 'Fruit', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Orange Juice', 'Juice', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Oregano', 'Herb', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Pancetta', 'Meat', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Paprika', 'Condiment', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Parmesan', 'Dairy', 7, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Parsley', 'Herb', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Passion Fruit', 'Fruit', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Pecorino', 'Fruit', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Pepperoni', 'Meat', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Pickle', 'Vegetable', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Pomegranate', 'Fruit', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Popcorn', 'Cereal', 2, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Porcini', 'Fungus', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Pork', 'Meat', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Potato', 'Vegetable', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Praline', 'Chocolate', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Prawn', 'Seafood', 3, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Prosciutto', 'Meat', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Provolone', 'Dairy', 7, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Pâté', 'Meat', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Quinoa', 'Herb', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Rice', 'Cereal', 2, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Ricotta', 'Dairy', 7, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Rocket', 'Herb', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Rosemary', 'Herb', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Saffron', 'Herb', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Salami', 'Meat', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Salmon', 'Seafood', 5, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Savoiardi', 'Dessert', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Sea Salt', 'Condiment', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Sesame', 'Herb', 12, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Soda', 'Water', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Sorbet', 'Dessert', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Spaghetti', 'Cereal', 2, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Spinach', 'Herb', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Stock', 'Water', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Sugar', 'Condiment', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Tagliatelle', 'Cereal', 2, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Tea', 'Beverage', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Tomato', 'Vegetable', 0, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Tortilla Chip', 'Snack', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Trout', 'Seafood', 5, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Truffle', 'Fungus', 0, 21)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Whipped Cream', 'Dairy', 7, 15)
  INTO Ingredients (Name, Kind, Allergens, DPH) VALUES ('Yeast', 'Yeast', 0, 15)
SELECT * FROM DUAL;

INSERT ALL
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Bread Board', 'Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Bread Board', 'Yeast')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Bread Board', 'Black Olive')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Bread Board', 'Caper')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Bread Board', 'Olive Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Big Green Olives on Ice', 'Green Olive')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Big Green Olives on Ice', 'Black Olive')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Big Green Olives on Ice', 'Caper')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Big Green Olives on Ice', 'Olive Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Big Green Olives on Ice', 'Yeast')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Cheese Croquettes', 'Potato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Cheese Croquettes', 'Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Cheese Croquettes', 'Fontina')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Cheese Croquettes', 'Milk')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Cheese Croquettes', 'Butter')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Cheese Croquettes', 'Egg')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Crispy Polenta Chips', 'Cornmeal')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Crispy Polenta Chips', 'Stock')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Crispy Polenta Chips', 'Parmesan')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Crispy Polenta Chips', 'Rosemary')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Nachos', 'Tortilla Chip')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Nachos', 'Ricotta')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Nachos', 'Cream Cheese')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Nachos', 'Mozzarella')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Nachos', 'Provolone')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Nachos', 'Egg')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Nachos', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Nachos', 'Chilli')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Nachos', 'Garlic')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Nachos', 'Olive Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Creamy Burrata', 'Burrata')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Creamy Burrata', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Creamy Burrata', 'Rocket')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Creamy Burrata', 'Olive Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Buttermilk Squid', 'Calamari')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Buttermilk Squid', 'Buttermilk')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Buttermilk Squid', 'Rocket')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Buttermilk Squid', 'Lemon')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Buttermilk Squid', 'Garlic')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Buttermilk Squid', 'Olive Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Margherita Arancini', 'Rice')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Margherita Arancini', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Margherita Arancini', 'Mozzarella')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Margherita Arancini', 'Butter')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Margherita Arancini', 'Onion')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Margherita Arancini', 'Garlic')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Margherita Arancini', 'Basil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meatballs', 'Beef')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meatballs', 'Garlic')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meatballs', 'Olive Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meatballs', 'Egg')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meatballs', 'Parsley')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meatballs', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meatballs', 'Basil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meatballs', 'Parmesan')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Vegetable Antipasti Plate', 'Courgette')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Vegetable Antipasti Plate', 'Aubergine')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Vegetable Antipasti Plate', 'Mozzarella')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Vegetable Antipasti Plate', 'Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Vegetable Antipasti Plate', 'Yeast')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Vegetable Antipasti Plate', 'Olive')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Vegetable Antipasti Plate', 'Pickle')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meat Plank', 'Prosciutto')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meat Plank', 'Salami')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meat Plank', 'Olive Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meat Plank', 'Mortadella')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meat Plank', 'Mozzarella')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meat Plank', 'Pecorino')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meat Plank', 'Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meat Plank', 'Yeast')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meat Plank', 'Olive')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Meat Plank', 'Pickle')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tagliatelle Bolognese', 'Tagliatelle')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tagliatelle Bolognese', 'Pork')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tagliatelle Bolognese', 'Beef')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tagliatelle Bolognese', 'Onion')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tagliatelle Bolognese', 'Carrot')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tagliatelle Bolognese', 'Celery')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tagliatelle Bolognese', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tagliatelle Bolognese', 'Parmesan')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Lasagne', 'Lasagne')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Lasagne', 'Pork')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Lasagne', 'Beef')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Lasagne', 'Onion')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Lasagne', 'Carrot')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Lasagne', 'Celery')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Lasagne', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Lasagne', 'Butter')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Lasagne', 'Milk')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Lasagne', 'Mozzarella')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Prawn Linguine', 'Linguine')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Prawn Linguine', 'Prawn')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Prawn Linguine', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Prawn Linguine', 'Fennel')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Prawn Linguine', 'Saffron')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Prawn Linguine', 'Chilli')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Prawn Linguine', 'Rocket')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Prawn Linguine', 'Garlic')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Carbonara', 'Casarecce')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Carbonara', 'Pancetta')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Carbonara', 'Parmesan')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Carbonara', 'Egg')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Spaghetti & Meatballs', 'Spaghetti')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Spaghetti & Meatballs', 'Pork')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Spaghetti & Meatballs', 'Garlic')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Spaghetti & Meatballs', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Spaghetti & Meatballs', 'Basil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Spaghetti & Meatballs', 'Parmesan')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Gnocchi Pomodoro', 'Potato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Gnocchi Pomodoro', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Gnocchi Pomodoro', 'Garlic')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Gnocchi Pomodoro', 'Basil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Gnocchi Pomodoro', 'Mozzarella')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Spinach & Ricotta Ravioli', 'Egg')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Spinach & Ricotta Ravioli', 'Ricotta')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Spinach & Ricotta Ravioli', 'Spinach')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Spinach & Ricotta Ravioli', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Spinach & Ricotta Ravioli', 'Mozzarella')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Spinach & Ricotta Ravioli', 'Basil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Mushroom Ravioli', 'Egg')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Mushroom Ravioli', 'Fontina')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Mushroom Ravioli', 'Porcini')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Mushroom Ravioli', 'Parmesan')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Octopus Ravioli', 'Egg')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Octopus Ravioli', 'Octopus')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Octopus Ravioli', 'Mollusc')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Octopus Ravioli', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Octopus Ravioli', 'Crab')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Grilled Chicken Under A Brick', 'Chicken')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Grilled Chicken Under A Brick', 'Basil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Grilled Chicken Under A Brick', 'Parmesan')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Grilled Chicken Under A Brick', 'Olive Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Grilled Chicken Under A Brick', 'Rocket')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Grilled Chicken Under A Brick', 'Lemon')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Truffled Chicken Under A Brick', 'Chicken')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Truffled Chicken Under A Brick', 'Mushroom')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Truffled Chicken Under A Brick', 'Truffle')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Truffled Chicken Under A Brick', 'Lemon')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Burger', 'Beef')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Burger', 'Egg')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Burger', 'Butter')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Burger', 'Milk')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Burger', 'Cream')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Burger', 'Onion')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Burger', 'Cheddar')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Burger', 'Pancetta')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Italian Burger', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Veggie Quinoa & Kale Burger', 'Quinoa')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Veggie Quinoa & Kale Burger', 'Kale')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Veggie Quinoa & Kale Burger', 'Onion')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Veggie Quinoa & Kale Burger', 'Cottage Cheese')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Veggie Quinoa & Kale Burger', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Veggie Quinoa & Kale Burger', 'Rocket')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('British Trout', 'Trout')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('British Trout', 'Crab')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('British Trout', 'Mollusc')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('British Trout', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('British Trout', 'Olive Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('British Trout', 'Garlic')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Flash Steak', 'Beef')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Flash Steak', 'Rocket')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Flash Steak', 'Parmesan')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Sirloin Steak', 'Beef Steak')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Sirloin Steak', 'Mushroom')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Sirloin Steak', 'Lemon')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Sirloin Steak', 'Garlic')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Sirloin Steak', 'Parsley')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Sirloin Steak', 'Rocket')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Salad', 'Avocado')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Salad', 'Beetroot')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Salad', 'Broccoli')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Salad', 'Pomegranate')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Salad', 'Harissa')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Salad', 'Nuts')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Salad', 'Sesame')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Salad', 'Garlic')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Salad', 'Chilli')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Kale Salad', 'Lentils')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Kale Salad', 'Avocado')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Kale Salad', 'Kale')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Kale Salad', 'Pomegranate')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Kale Salad', 'Grape')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Kale Salad', 'Lemon')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Kale Salad', 'Olive Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Julietta', 'Yeast')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Julietta', 'Olive Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Julietta', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Julietta', 'Basil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Julietta', 'Mozzarella')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Julietta', 'Parmesan')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Pepperoni', 'Yeast')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Pepperoni', 'Olive Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Pepperoni', 'Pepperoni')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Pepperoni', 'Salami')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Pepperoni', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Pepperoni', 'Mozzarella')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Pepperoni', 'Oregano')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Four Cheese', 'Yeast')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Four Cheese', 'Olive Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Four Cheese', 'Pepperoni')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Four Cheese', 'Salami')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Four Cheese', 'Tomato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Four Cheese', 'Mozzarella')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Pizza Four Cheese', 'Oregano')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Sweet Potato Fries', 'Potato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Sweet Potato Fries', 'Chilli')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Sweet Potato Fries', 'Cumin')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Sweet Potato Fries', 'Paprika')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Sweet Potato Fries', 'Brown Sugar')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Kale & Hazelnut Salad', 'Kale')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Kale & Hazelnut Salad', 'Hazelnut')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Kale & Hazelnut Salad', 'Milk')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Kale & Hazelnut Salad', 'Mustard')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Roasted Broccoli', 'Broccoli')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Roasted Broccoli', 'Olive Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Roasted Broccoli', 'Lemon')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Skinny Fries', 'Potato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Skinny Fries', 'Sea Salt')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Molten Chocolate Praline Pudding', 'Gelato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Molten Chocolate Praline Pudding', 'Praline')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Molten Chocolate Praline Pudding', 'Chocolate')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Chocolate Brownie', 'Butter')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Chocolate Brownie', 'Egg')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Chocolate Brownie', 'Sugar')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Chocolate Brownie', 'Chocolate')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Chocolate Brownie', 'Gelato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Chocolate Brownie', 'Popcorn')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Baked Lemon & Ricotta Cheesecake', 'Ricotta')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Baked Lemon & Ricotta Cheesecake', 'Lemon')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Baked Lemon & Ricotta Cheesecake', 'Sugar')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Baked Lemon & Ricotta Cheesecake', 'Egg')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Baked Lemon & Ricotta Cheesecake', 'Blueberry')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Torta Di Nada', 'Egg')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Torta Di Nada', 'Sugar')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Torta Di Nada', 'Butter')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Torta Di Nada', 'Olive Oil')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Torta Di Nada', 'Milk')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Torta Di Nada', 'Blueberry')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Torta Di Nada', 'Whipped Cream')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Torta Di Nada', 'Honey')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Torta Di Nada', 'Orange')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tiramisù', 'Savoiardi')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tiramisù', 'Chocolate')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tiramisù', 'Mascarpone')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tiramisù', 'Coffee')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tiramisù', 'Espresso')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tiramisù', 'Egg')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tiramisù', 'Sugar')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tiramisù', 'Milk')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tiramisù', 'Cocoa')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Affogato', 'Gelato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Affogato', 'Espresso')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Gelato & Sorbet', 'Gelato')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Gelato & Sorbet', 'Sorbet')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Fruit Juice', 'Cranberry Juice')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Fruit Juice', 'Apple Juice')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Fruit Juice', 'Orange Juice')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Homemade Lemonade', 'Lemon')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Homemade Lemonade', 'Sugar')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Homemade Lemonade', 'Mint')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Homemade Lemonade', 'Soda')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Espresso', 'Espresso')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Cappuccino', 'Cappuccino')
  INTO DishesIngredients (Dish, Ingredient) VALUES ('Tea', 'Tea')
SELECT * FROM DUAL;

INSERT ALL
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (1, '06-03-2018 20:24:53', 0, 'maydwell@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (2, '05-03-2018 13:26:04', 0, 'pavey@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (3, '05-08-2017 02:24:17', 0, 'haffner@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (4, '23-03-2018 22:16:23', 0, 'maydwell@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (5, '20-09-2017 05:04:30', 0, 'haffner@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (6, '11-05-2017 20:43:27', 0, 'pavey@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (7, '18-05-2017 16:59:51', 0, 'haffner@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (8, '16-12-2017 14:54:06', 0, 'clarkin@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (9, '06-02-2018 21:07:44', 0, 'maydwell@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (10, '07-02-2018 09:50:07', 0, 'dando@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (11, '26-12-2017 12:00:06', 0, 'maydwell@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (12, '08-12-2017 06:08:27', 0, 'martinon@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (13, '02-05-2017 04:07:26', 0, 'pavey@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (14, '08-11-2017 00:42:44', 0, 'rycroft@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (15, '02-12-2017 21:16:41', 0, 'rycroft@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (16, '25-01-2018 08:59:29', 0, 'robardey@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (17, '03-10-2017 22:55:43', 0, 'maydwell@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (18, '17-12-2017 00:58:19', 0, 'rycroft@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (19, '25-07-2017 03:52:27', 0, 'maydwell@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (20, '19-09-2017 11:20:34', 0, 'pavey@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (21, '11-05-2017 22:52:32', 1, 'haffner@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (22, '14-04-2017 11:39:01', 0, 'clarkin@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (23, '26-03-2018 08:22:42', 1, 'wrightem@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (24, '26-01-2018 12:45:11', 1, 'haffner@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (25, '06-04-2017 19:10:15', 0, 'dando@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (26, '20-04-2017 18:24:45', 0, 'martinon@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (27, '02-07-2017 09:42:01', 0, 'dando@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (28, '15-01-2018 04:50:20', 0, 'clarkin@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (29, '22-05-2017 06:13:40', 0, 'martinon@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (30, '20-05-2017 18:29:45', 0, 'macphail@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (31, '05-12-2017 12:39:16', 0, 'dando@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (32, '08-09-2017 02:33:54', 1, 'robardey@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (33, '19-12-2017 06:09:30', 0, 'clarkin@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (34, '06-02-2018 05:58:18', 0, 'clarkin@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (35, '18-05-2017 01:48:59', 0, 'martinon@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (36, '18-06-2017 13:06:07', 0, 'martinon@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (37, '20-10-2017 08:39:33', 0, 'wrightem@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (38, '11-12-2017 07:05:51', 0, 'wrightem@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (39, '20-12-2017 19:03:09', 1, 'maydwell@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (40, '07-04-2017 08:26:37', 0, 'pavey@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (41, '14-04-2017 13:42:22', 0, 'martinon@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (42, '10-07-2017 12:26:31', 1, 'robardey@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (43, '28-02-2018 00:15:22', 0, 'dando@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (44, '07-09-2017 08:17:10', 0, 'wrightem@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (45, '20-03-2018 16:22:46', 1, 'wrightem@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (46, '17-04-2017 19:04:38', 0, 'martinon@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (47, '08-02-2018 17:47:31', 1, 'maydwell@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (48, '30-11-2017 17:36:30', 0, 'dando@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (49, '02-10-2017 23:08:13', 0, 'haffner@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (50, '07-12-2017 11:07:55', 0, 'maydwell@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (51, '21-07-2017 17:46:36', 1, 'martinon@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (52, '19-07-2017 10:42:10', 0, 'macphail@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (53, '13-05-2017 01:48:32', 0, 'robardey@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (54, '15-11-2017 09:28:33', 0, 'robardey@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (55, '14-08-2017 20:39:40', 0, 'rycroft@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (56, '03-05-2017 09:05:14', 0, 'pavey@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (57, '12-11-2017 22:50:03', 1, 'robardey@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (58, '22-03-2018 20:21:48', 0, 'wrightem@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (59, '16-06-2017 20:10:16', 0, 'rycroft@restaurant.com')
  INTO Bills (BillNumber, DateAndTime, IsReserved, Employee) VALUES (60, '01-07-2017 00:35:03', 0, 'robardey@restaurant.com')
SELECT * FROM DUAL;

INSERT ALL
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('14-03-2018 14:11:04', 'maydwell@restaurant.com', 1)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('26-03-2018 14:07:40', 'pavey@restaurant.com', 2)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('10-03-2018 20:27:27', 'haffner@restaurant.com', 3)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('18-03-2018 07:28:16', 'maydwell@restaurant.com', 4)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('12-03-2018 05:03:53', 'haffner@restaurant.com', 5)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('25-03-2018 19:00:51', 'pavey@restaurant.com', 6)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('29-03-2018 12:14:33', 'haffner@restaurant.com', 7)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('08-03-2018 21:32:52', 'clarkin@restaurant.com', 8)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('15-03-2018 17:26:50', 'maydwell@restaurant.com', 9)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('06-03-2018 13:49:00', 'dando@restaurant.com', 10)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('28-03-2018 18:47:21', 'maydwell@restaurant.com', 11)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('02-03-2018 20:03:29', 'martinon@restaurant.com', 12)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('09-03-2018 02:32:10', 'pavey@restaurant.com', 13)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('13-03-2018 17:14:13', 'rycroft@restaurant.com', 14)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('21-03-2018 00:56:59', 'rycroft@restaurant.com', 15)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('25-03-2018 04:52:54', 'robardey@restaurant.com', 16)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('20-03-2018 11:41:34', 'maydwell@restaurant.com', 17)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('14-03-2018 18:55:57', 'rycroft@restaurant.com', 18)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('27-03-2018 12:50:22', 'maydwell@restaurant.com', 19)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('15-03-2018 15:52:45', 'pavey@restaurant.com', 20)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('10-03-2018 06:22:45', 'haffner@restaurant.com', 21)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('06-03-2018 18:05:17', 'clarkin@restaurant.com', 22)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('02-03-2018 06:53:21', 'wrightem@restaurant.com', 23)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('21-03-2018 16:26:20', 'haffner@restaurant.com', 24)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('29-03-2018 03:11:20', 'dando@restaurant.com', 25)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('10-03-2018 12:56:27', 'martinon@restaurant.com', 26)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('22-03-2018 08:59:47', 'dando@restaurant.com', 27)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('17-03-2018 08:46:01', 'clarkin@restaurant.com', 28)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('17-03-2018 00:04:49', 'martinon@restaurant.com', 29)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('04-03-2018 12:54:59', 'macphail@restaurant.com', 30)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('28-03-2018 13:33:55', 'dando@restaurant.com', 31)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('17-03-2018 17:06:33', 'robardey@restaurant.com', 32)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('29-03-2018 12:25:37', 'clarkin@restaurant.com', 33)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('16-03-2018 13:37:46', 'clarkin@restaurant.com', 34)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('08-03-2018 20:10:24', 'martinon@restaurant.com', 35)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('12-03-2018 08:02:09', 'martinon@restaurant.com', 36)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('13-03-2018 10:39:46', 'wrightem@restaurant.com', 37)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('29-03-2018 13:40:27', 'wrightem@restaurant.com', 38)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('25-03-2018 10:27:45', 'maydwell@restaurant.com', 39)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('15-03-2018 16:16:16', 'pavey@restaurant.com', 40)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('15-03-2018 03:58:40', 'martinon@restaurant.com', 41)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('16-03-2018 06:29:03', 'robardey@restaurant.com', 42)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('24-03-2018 18:24:35', 'dando@restaurant.com', 43)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('02-03-2018 07:17:41', 'wrightem@restaurant.com', 44)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('02-03-2018 23:48:20', 'wrightem@restaurant.com', 45)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('18-03-2018 09:42:58', 'martinon@restaurant.com', 46)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('13-03-2018 03:46:26', 'maydwell@restaurant.com', 47)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('16-03-2018 09:39:17', 'dando@restaurant.com', 48)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('30-03-2018 01:59:47', 'haffner@restaurant.com', 49)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('17-03-2018 22:15:55', 'maydwell@restaurant.com', 50)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('03-03-2018 13:47:57', 'martinon@restaurant.com', 51)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('02-03-2018 09:56:00', 'macphail@restaurant.com', 52)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('15-03-2018 18:41:29', 'robardey@restaurant.com', 53)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('04-03-2018 15:40:31', 'robardey@restaurant.com', 54)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('23-03-2018 07:59:37', 'rycroft@restaurant.com', 55)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('05-03-2018 16:17:56', 'pavey@restaurant.com', 56)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('07-03-2018 10:21:19', 'robardey@restaurant.com', 57)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('28-03-2018 23:12:33', 'wrightem@restaurant.com', 58)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('13-03-2018 03:22:27', 'rycroft@restaurant.com', 59)
  INTO Orders (DateAndTime, Employee, BillNumber) VALUES ('01-03-2018 10:30:00', 'robardey@restaurant.com', 60)
SELECT * FROM DUAL;

INSERT ALL
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (15, 1)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (16, 2)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (9, 3)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (3, 4)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (1, 5)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (14, 6)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (6, 7)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (3, 8)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (5, 9)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (15, 10)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (6, 11)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (10, 12)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (11, 13)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (4, 14)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (5, 15)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (3, 16)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (14, 17)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (16, 18)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (8, 19)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (5, 20)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (4, 21)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (15, 22)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (4, 23)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (5, 24)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (10, 25)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (6, 26)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (14, 27)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (7, 28)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (15, 29)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (5, 30)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (7, 31)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (16, 32)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (2, 33)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (7, 34)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (2, 35)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (3, 36)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (8, 37)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (1, 38)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (13, 39)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (9, 40)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (9, 41)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (13, 42)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (15, 43)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (1, 44)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (2, 45)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (10, 46)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (13, 47)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (4, 48)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (15, 49)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (14, 50)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (6, 51)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (6, 52)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (2, 53)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (11, 54)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (7, 55)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (14, 56)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (8, 57)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (2, 58)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (15, 59)
  INTO TablesOrders (TableNumber, OrderNumber) VALUES (14, 60)
SELECT * FROM DUAL;

INSERT ALL
  INTO OrdersDishes (OrderNumber, Dish) VALUES (1, 'Espresso')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (1, 'Tiramisù')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (1, 'Kale & Hazelnut Salad')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (2, 'Fruit Juice')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (2, 'Italian Burger')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (2, 'Baked Lemon & Ricotta Cheesecake')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (3, 'Baked Lemon & Ricotta Cheesecake')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (3, 'Torta Di Nada')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (3, 'Tagliatelle Bolognese')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (4, 'Pizza Four Cheese')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (4, 'Chocolate Brownie')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (4, 'Italian Nachos')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (5, 'Carbonara')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (5, 'Spinach & Ricotta Ravioli')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (5, 'Meatballs')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (6, 'Italian Burger')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (6, 'Grilled Chicken Under A Brick')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (6, 'British Trout')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (7, 'Meatballs')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (7, 'Carbonara')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (7, 'Grilled Chicken Under A Brick')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (8, 'Grilled Chicken Under A Brick')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (8, 'Sirloin Steak')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (8, 'Torta Di Nada')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (9, 'Tea')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (9, 'Cheese Croquettes')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (9, 'Gnocchi Pomodoro')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (10, 'Tiramisù')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (10, 'Baked Lemon & Ricotta Cheesecake')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (10, 'Vegetable Antipasti Plate')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (11, 'Gnocchi Pomodoro')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (11, 'Octopus Ravioli')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (11, 'Salad')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (12, 'Tea')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (12, 'Molten Chocolate Praline Pudding')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (12, 'Baked Lemon & Ricotta Cheesecake')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (13, 'Prawn Linguine')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (13, 'Meat Plank')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (13, 'Pizza Four Cheese')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (14, 'Margherita Arancini')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (14, 'Big Green Olives on Ice')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (14, 'Roasted Broccoli')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (15, 'Italian Burger')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (15, 'Sirloin Steak')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (15, 'Homemade Lemonade')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (16, 'Baked Lemon & Ricotta Cheesecake')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (16, 'Truffled Chicken Under A Brick')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (16, 'Gnocchi Pomodoro')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (17, 'Pizza Four Cheese')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (17, 'Gnocchi Pomodoro')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (17, 'Cappuccino')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (18, 'Buttermilk Squid')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (18, 'Roasted Broccoli')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (18, 'Prawn Linguine')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (19, 'Italian Burger')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (19, 'Spaghetti & Meatballs')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (19, 'Meatballs')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (20, 'Molten Chocolate Praline Pudding')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (20, 'Kale & Hazelnut Salad')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (20, 'Tiramisù')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (21, 'Gnocchi Pomodoro')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (21, 'Affogato')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (21, 'Bread Board')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (22, 'Prawn Linguine')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (22, 'Pizza Julietta')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (22, 'Tiramisù')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (23, 'Baked Lemon & Ricotta Cheesecake')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (23, 'Truffled Chicken Under A Brick')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (23, 'Sweet Potato Fries')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (24, 'Sirloin Steak')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (24, 'Skinny Fries')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (24, 'Homemade Lemonade')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (25, 'Cappuccino')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (25, 'Affogato')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (25, 'Tea')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (26, 'Sweet Potato Fries')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (26, 'Gelato & Sorbet')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (26, 'Buttermilk Squid')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (27, 'Chocolate Brownie')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (27, 'Sweet Potato Fries')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (27, 'Pizza Pepperoni')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (28, 'Spinach & Ricotta Ravioli')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (28, 'British Trout')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (28, 'Salad')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (29, 'Vegetable Antipasti Plate')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (29, 'Salad')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (29, 'Fruit Juice')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (30, 'Veggie Quinoa & Kale Burger')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (30, 'Espresso')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (30, 'Gelato & Sorbet')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (31, 'Molten Chocolate Praline Pudding')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (31, 'Affogato')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (31, 'Skinny Fries')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (32, 'Torta Di Nada')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (32, 'Bread Board')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (32, 'Margherita Arancini')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (33, 'Vegetable Antipasti Plate')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (33, 'Tiramisù')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (33, 'Bread Board')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (34, 'Pizza Pepperoni')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (34, 'Veggie Quinoa & Kale Burger')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (34, 'Tagliatelle Bolognese')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (35, 'Torta Di Nada')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (35, 'Tagliatelle Bolognese')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (35, 'Tea')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (36, 'Salad')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (36, 'Buttermilk Squid')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (36, 'Kale & Hazelnut Salad')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (37, 'Cappuccino')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (37, 'Gelato & Sorbet')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (37, 'Torta Di Nada')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (38, 'Margherita Arancini')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (38, 'Sirloin Steak')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (38, 'Spaghetti & Meatballs')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (39, 'Sirloin Steak')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (39, 'Cappuccino')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (39, 'Kale Salad')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (40, 'Sirloin Steak')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (40, 'Cheese Croquettes')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (40, 'Meat Plank')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (41, 'Sirloin Steak')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (41, 'Spinach & Ricotta Ravioli')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (41, 'Kale & Hazelnut Salad')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (42, 'Carbonara')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (42, 'Prawn Linguine')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (42, 'Skinny Fries')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (43, 'Sirloin Steak')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (43, 'Roasted Broccoli')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (43, 'Flash Steak')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (44, 'Meat Plank')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (44, 'Molten Chocolate Praline Pudding')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (44, 'Torta Di Nada')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (45, 'Salad')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (45, 'Flash Steak')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (45, 'Pizza Julietta')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (46, 'Vegetable Antipasti Plate')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (46, 'Gnocchi Pomodoro')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (46, 'Molten Chocolate Praline Pudding')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (47, 'Meat Plank')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (47, 'Gelato & Sorbet')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (47, 'Meatballs')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (48, 'Pizza Pepperoni')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (48, 'Roasted Broccoli')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (48, 'Italian Burger')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (49, 'Torta Di Nada')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (49, 'Crispy Polenta Chips')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (49, 'Homemade Lemonade')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (50, 'Cappuccino')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (50, 'Gnocchi Pomodoro')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (50, 'Torta Di Nada')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (51, 'Margherita Arancini')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (51, 'Spaghetti & Meatballs')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (51, 'Crispy Polenta Chips')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (52, 'Vegetable Antipasti Plate')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (52, 'Creamy Burrata')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (52, 'Mushroom Ravioli')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (53, 'Veggie Quinoa & Kale Burger')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (53, 'Lasagne')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (53, 'Big Green Olives on Ice')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (54, 'Truffled Chicken Under A Brick')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (54, 'Kale Salad')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (54, 'Tagliatelle Bolognese')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (55, 'Carbonara')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (55, 'Creamy Burrata')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (55, 'Gelato & Sorbet')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (56, 'Vegetable Antipasti Plate')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (56, 'Truffled Chicken Under A Brick')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (56, 'Grilled Chicken Under A Brick')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (57, 'Vegetable Antipasti Plate')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (57, 'Italian Burger')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (57, 'Homemade Lemonade')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (58, 'Margherita Arancini')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (58, 'Truffled Chicken Under A Brick')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (58, 'Tea')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (59, 'Homemade Lemonade')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (59, 'Tiramisù')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (59, 'Baked Lemon & Ricotta Cheesecake')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (60, 'Big Green Olives on Ice')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (60, 'Octopus Ravioli')
  INTO OrdersDishes (OrderNumber, Dish) VALUES (60, 'British Trout')
SELECT * FROM DUAL;

-- SELECT queries.
-- 1st query with join of 2 tables:
-- Shows information about employees who created orders in a chosen date.
-- (Vypise informace o zamestnancich, ktere vytvorili objednavky v zadanem case.)
SELECT DISTINCT Email, FirstName, LastName, JobTitle
FROM Orders, Employees
WHERE Orders.Employee = Employees.Email
      AND DateAndTime BETWEEN '12-03-2018 00:00:00' AND '13-03-2018 00:00:00';
-- 2nd query with join of 2 tables:
-- Shows information about who and when reserved a chosen table.
-- (Vypise informace o tom, kdo a kdy zarezervoval zadany stul.)
SELECT FirstName, LastName, CAST(DateAndTime AS DATE)
FROM Customers, Reservations
WHERE Reservations.Customer = Customers.Email and TableNumber = 11;
-- 1 query with join of 3 tables:
-- Shows number of table which ordered a chosen dish at a chosen time.
-- (Vypise cislo stolu, ktery objednal zadane jidlo v zadany cas.
SELECT TableNumber
FROM TablesOrders, OrdersDishes, Orders
WHERE TablesOrders.OrderNumber = OrdersDishes.OrderNumber AND OrdersDishes.OrderNumber = Orders.OrderNumber
      AND Dish='Sirloin Steak' AND DateAndTime = '08-03-2018 21:32:52';
-- 1st query with GROUP BY
-- Shows number of employees in each job position.
-- (Vypise pocet zamestnancu na kazde pracovni pozici.)
SELECT JobTitle, count(JobTitle)
FROM Employees
GROUP BY JobTitle;
-- 2nd query with GROUP BY
-- Shows an amount of money earned from orders in a chosen time period.
-- (Vypise penize ziskane z objedavek za urcite obdobi.)
SELECT Sum(Price)
FROM Dishes, OrdersDishes, Orders
WHERE Dishes.Name = OrdersDishes.Dish AND OrdersDishes.OrderNumber = Orders.OrderNumber
      AND DateAndTime BETWEEN '01-03-2018 00:00:00' AND '31-03-2018 00:00:00';
-- 1 query with EXISTS
SELECT FirstName,LastName
FROM Customers
WHERE EXISTS
(
  SELECT Employees.Email
  FROM Employees
  WHERE Employees.Email = Customers.Email
);
-- 1 query with IN and nested SELECT:
-- Shows when and which dishes were ordered that contain seafood.
-- (Vypise kdy byli objednany jidla s morskymi produkty a jejich nazvy.)
SELECT CAST(DateAndTime AS DATE), Dish
FROM OrdersDishes, Orders
WHERE OrdersDishes.OrderNumber = Orders.OrderNumber AND OrdersDishes.Dish IN (
  SELECT Dish
  FROM DishesIngredients, Ingredients
  WHERE DishesIngredients.Ingredient = Ingredients.Name AND Ingredients.Kind = 'Seafood');

-- Examples of triggers usage.
-- PickFreeTable trigger.
SELECT *
FROM Reservations
WHERE DateAndTime = '20-12-2017 19:03:09';

INSERT INTO Reservations (ReservationID, DateAndTime, Customer)
VALUES (25, '20-12-2017 19:03:09', 'dmcwilliams1@reference.com');

SELECT *
FROM Reservations
WHERE DateAndTime = '20-12-2017 19:03:09';

-- Examples of procedures usage.
SELECT Dish, Ingredient, Price
FROM Dishes D JOIN DishesIngredients DI ON D.Name = DI.Dish
WHERE Ingredient = 'Beef';

BEGIN
  ApplyDiscount(10, 'Beef');
END;
/

SELECT Dish, Ingredient, Price
FROM Dishes D JOIN DishesIngredients DI ON D.Name = DI.Dish
WHERE Ingredient = 'Beef';

BEGIN
  GetReservationsInfo('Derry', 'McWilliams');
END;
/

-- EXPLAIN PLAN for the query that shows avg price of an order without using index.
EXPLAIN PLAN FOR
SELECT OD.OrderNumber, ROUND(AVG(Price), 1)
FROM OrdersDishes OD JOIN Dishes D on OD.Dish = D.Name
GROUP BY OD.OrderNumber
ORDER BY OD.OrderNumber;
SELECT * FROM TABLE(DBMS_XPLAN.DISPLAY);

-- Create an explicit index for the dishes and their prices.
CREATE INDEX DishPriceI ON DISHES(NAME, PRICE);

-- EXPLAIN PLAN for the query that shows avg price of an order using index.
EXPLAIN PLAN FOR
SELECT /*+ INDEX(D DishPriceI) */ OD.OrderNumber, ROUND(AVG(Price), 1)
FROM OrdersDishes OD JOIN Dishes D on OD.Dish = D.Name
GROUP BY OD.OrderNumber
ORDER BY OD.OrderNumber;
SELECT * FROM TABLE(DBMS_XPLAN.DISPLAY);

-- Grant access rights to the user, so he can create Materialized View.
GRANT SELECT, UPDATE, INSERT, DELETE ON Orders TO XBOCKP00;
GRANT SELECT, UPDATE, INSERT, DELETE ON OrdersDishes TO XBOCKP00;
GRANT ALL ON DishesIngredients TO XBOCKP00;
GRANT ALL ON Ingredients TO XBOCKP00;
GRANT EXECUTE ON GetReservationsInfo TO XBOCKP00;

-- Create logs on Master Tables that are used in Materialized View
-- to support REFRESH FAST.
CREATE MATERIALIZED VIEW LOG ON DishesIngredients WITH ROWID, SEQUENCE INCLUDING NEW VALUES;
CREATE MATERIALIZED VIEW LOG ON Ingredients WITH ROWID, SEQUENCE INCLUDING NEW VALUES;

-- Create view that shows information about Dishes and ingredients they contain.
CREATE MATERIALIZED VIEW DishesIngredientsInfoMV
REFRESH FAST ON COMMIT
AS
  SELECT DI.ROWID AS ROWIDDI, I.ROWID AS ROWIDI, Dish, Ingredient, Kind, Allergens
  FROM DishesIngredients DI, Ingredients I
  WHERE DI.Ingredient = I.Name;

GRANT SELECT ON DishesIngredientsInfoMV TO XBOCKP00;

COMMIT;

-- Example of Materialized View usage.
SELECT Dish, Ingredient, Kind, Allergens FROM DishesIngredientsInfoMV
WHERE Dish = 'Gin Fizz';

INSERT INTO Dishes VALUES ('Gin Fizz', 0, 8, 'rycroft@restaurant.com');
INSERT INTO Ingredients VALUES ('Gin', 'Alcohol', 0, 21);
INSERT INTO Ingredients VALUES ('Elderflower', 'Herb', 0, 21);
INSERT INTO Ingredients VALUES ('Limoncello', 'Alcohol', 0, 21);
INSERT INTO DishesIngredients (Dish, Ingredient) VALUES ('Gin Fizz', 'Gin');
INSERT INTO DishesIngredients (Dish, Ingredient) VALUES ('Gin Fizz', 'Elderflower');
INSERT INTO DishesIngredients (Dish, Ingredient) VALUES ('Gin Fizz', 'Limoncello');
INSERT INTO DishesIngredients (Dish, Ingredient) VALUES ('Gin Fizz', 'Lemon');
COMMIT;

SELECT Dish, Ingredient, Kind, Allergens FROM DishesIngredientsInfoMV
WHERE Dish = 'Gin Fizz';
